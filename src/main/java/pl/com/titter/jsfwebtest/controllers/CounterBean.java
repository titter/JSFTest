/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.titter.jsfwebtest.controllers;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Dominik
 */
@Entity
@ManagedBean(name = "CB")
@SessionScoped
public class CounterBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private Integer counter;

    public Integer getCounter() {
        return counter++;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CounterBean(){
        this.counter = 0;
    }

    @Override
    public String toString() {
        return "pl.com.titter.jsfwebtest.controllers.CounterBean[ id=" + id + " ]";
    }
    
}
