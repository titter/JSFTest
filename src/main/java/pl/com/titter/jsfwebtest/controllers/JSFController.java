/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.titter.jsfwebtest.controllers;


import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "JSFController")
@ApplicationScoped
public class JSFController {
    
    private String version = "alfa";
    private List<EntryItem> entries = new ArrayList<EntryItem>();
    private final EntityManagerFactory emf;
    private final EntityManager em;

    public void setEntries(List<EntryItem> entries) {
        this.entries = entries;
    }

    /**
     * Creates a new instance of JSFController
     */
    public JSFController() {
        emf = Persistence.createEntityManagerFactory("pu");
        em = emf.createEntityManager();
    }
    
    public EntityManager getEntityManager(){
        return em;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    
}
