/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.titter.jsfwebtest.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import pl.com.titter.jsfwebtest.User;

/**
 *
 * @author Dominik
 */
@ManagedBean(name = "LoginController")
@SessionScoped
public class LoginController implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private User user = new User();
    
    @ManagedProperty(value = "#{JSFController}")
    private JSFController jsf;
    private EntityManager em;
    
    /**
     * Creates a new instance of LoginController
     */
    public LoginController() {
        
    }
    
    private void persistUser(User u){
        EntityTransaction t = em.getTransaction();
        try {
            t.begin();
            List<User> list = em.createQuery("from User").getResultList();
            list.forEach(tmp->{System.out.println(tmp);});
            System.out.println("USER: "+u);
            if (!list.contains(u)){
                em.persist(u);

            }else{
                System.out.println("JUŻ JEST TAKI KOLO :)");
            }
            t.commit();
        } catch (Exception e) {
            e.printStackTrace();
            t.rollback();
        }
    }
    
    public void login(User u){
        System.out.println("ZALOGOWANO!");
        u.setLogged(Boolean.TRUE);
        u.setLogin(u.getEmail().split("@")[0]);
        persistUser(u);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public JSFController getJsf() {
        return jsf;
    }

    public void setJsf(JSFController jsf) {
        this.jsf = jsf;
    }
    
    @PreDestroy
    public void logout(User u){
        u.setLogged(Boolean.FALSE);
        persistUser(u);
    }
    
    @PostConstruct
    public void init(){
        em = jsf.getEntityManager();
    }
    
}
