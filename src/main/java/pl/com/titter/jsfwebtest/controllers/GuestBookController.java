/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.titter.jsfwebtest.controllers;

import static java.rmi.server.LogStream.log;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "GuestBookController")
@RequestScoped
public class GuestBookController {
    @ManagedProperty(value = "#{JSFController}")
    private JSFController jsf;
    @ManagedProperty(value = "#{CB}")
    private CounterBean cb;
    
    private EntryItem ei = new EntryItem();

    public CounterBean getCb() {
        return cb;
    }

    public void setCb(CounterBean cb) {
        this.cb = cb;
    }

    
    public EntryItem getEi() {
        System.out.println("EI GET");
        return ei;
    }

    public void setEi(EntryItem ei) {
        System.out.println("EI SET");
        this.ei = ei;
    }

    public JSFController getJsf() {
        System.out.println("JSF GET");
        return jsf;
    }

    public void setJsf(JSFController jsf) {
        System.out.println("JSF SET");
        this.jsf = jsf;
    }
    
    public GuestBookController() {
    }
    
    public List<EntryItem> getEntries(){
        System.out.println("GETTING...");
        
        EntityManager em = jsf.getEntityManager();
        EntityTransaction t = em.getTransaction();
        List<EntryItem> list = new ArrayList<EntryItem>();
        try{
            t.begin();
            Query q = em.createQuery("from entryItem");
            list = q.getResultList();
            t.commit();
        }catch(Exception e){
            e.printStackTrace();
            t.rollback();
        }
        return list;
    }
    
    public void save(EntryItem newEI){
        System.out.println("SAVING...");
        
        EntityManager em = jsf.getEntityManager();
        EntityTransaction t = em.getTransaction();
        try{
            t.begin();
            em.persist(newEI);
            t.commit();
        }catch(Exception e){
            e.printStackTrace();
            t.rollback();
        }finally{
            newEI.setEditable(false);
        }     
    }
    
    public String editAction(EntryItem e){
        e.setEditable(true);
	return null;
    }
    
    public void saveAction(List<EntryItem> lei){
        for(EntryItem e: lei){
            if (e!=null){
                if (e.getEditable())
                    save(e);
                e.setEditable(false);
            } 
        }
    }
    
    public void update(EntryItem newEI){
        newEI.setEditDate(Date.from(Instant.now()));
        save(newEI);
    }
}
