/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.titter.jsfwebtest.controllers;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author RENT
 */
@Entity(name = "entryItem")
public class EntryItem implements Serializable {
    private String name;
    private String content;
    private Date entryDate;
    private Date editDate;
    private Boolean editable;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    public EntryItem(){
        this.name = "";
        this.content = "";
        this.entryDate = Date.from(Instant.now());
    }
    
    public EntryItem(String newName, String newContent, Date newDate){
        this.name = newName;
        this.content = newContent;
        this.entryDate = newDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Date getEditDate() {
        return editDate;
    }

    public void setEditDate(Date editDate) {
        this.editDate = editDate;
    }
    
    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }
    
    public String toString(){
        return "["+this.id+", "+this.name+", "+this.entryDate+"]";
    }
}
